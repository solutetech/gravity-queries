jQuery(document).ready( function($) {

	$('[data-toggle="popover"]').popover();
	
	jQuery('#getbusiness').click(function() {

		jQuery('#getbusiness').html('<i class="fa fa-cog fa-spin fa-lg"></i>');
		var type = jQuery("#type").val();

		$.ajax({
			url: getbusiness.ajax_url,
			type : 'post',
			data : {
				action : 'getAllBusiness',
				type : type
			},
			success: function( data ) {
				jQuery('#getbusiness').html('Search');
				jQuery('#listbusiness').html(data);
			}
		});
	});
});