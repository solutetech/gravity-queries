<?php
/*
 Plugin Name: Gravity Queries
 Plugin URI: 
 Description: Helps queries thru Gravity Form enteries
 Version: 1.2
 Author: Mr. Wonderful
 Author URI: http://fletchlab.com
 License: GPL2
*/

function getAllBusiness() {

	$form_id = 2;
	$search_criteria = array();
	$sorting = array( 'key' => "14", 'direction' => "DESC" );
    	$entries = GFAPI::get_entries( $form_id, $search_criteria, $sorting);
	$type = $_POST['type'];

     	$row = "<h3 id='prof-subtitle'>Industry: " . $type . "</h3>";

     	foreach($entries as $entry)
     	{
	     	if($entry[8] == $type)
             	{

	              $row .= "<div class='col-sm-6 card-break'><strong>Name: " . $entry[4] . "</strong><br />" ;
                      $row .= "<strong>Website: </strong><a href='". rgar($entry, "5") ."' target='_blank'>" . rgar($entry, "5") . "</a><br />";
                      $row .= "<strong>Phone: </strong>" . rgar($entry, "6") . "<hr /></div>";                   	

             	}
     	}
     	echo substr($row, 0 , -1);
}


add_action( 'wp_enqueue_scripts', 'ajax_business_scripts' );

function ajax_business_scripts() {
	wp_enqueue_script( 'business', plugins_url( '/business.js', __FILE__ ), array('jquery'), '1.0', true );
	wp_localize_script( 'business', 'getbusiness', array('ajax_url' => admin_url( 'admin-ajax.php' )));
}

add_action('wp_ajax_getAllBusiness', 'getAllBusiness');  
add_action('wp_ajax_nopriv_getAllBusiness', 'getAllBusiness');
?>